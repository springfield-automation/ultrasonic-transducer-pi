import { Gpio } from 'onoff';
import { Transducer } from '@springfield/ultrasonic-transducer';

export class TransducerPi extends Transducer {
  private enable: Gpio;

  constructor(enablePin: number) {
    super('/dev/serial0')
    this.enable = new Gpio(enablePin, 'out');
    this.enable.writeSync(0);
  }

  public async getCurrentValue(): Promise<number> {
    this.enable.writeSync(1);
    const result = await super.getCurrentValue();
    this.enable.writeSync(0);
    return result;
  }
}
