const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const { TransducerPi } = require('../dist/transducer.js');

let transducer = null;

chai.use(chaiAsPromised);
chai.should();

before(() => {
  transducer = new TransducerPi(17);
});

after(() => {
  if(transducer) {
    transducer.close();
  }
});

describe('transducer', () => {
  it('reads value >= 30', () => {
    return transducer.getCurrentValue().should.eventually.be.at.least(30);
  });
});