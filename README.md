# Overview

This module extends the ultrasonic-transducer module and adds the ability for a RaspberryPi to control the output enable on the transducer.